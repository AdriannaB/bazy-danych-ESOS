using Esos.Web.Infrastructure.DTO;

namespace Esos.Web.Infrastructure.Services.Interfaces
{
    public interface IAccountService
    {
         User LogIn(string login, string password);
    }
}