using System.Collections.Generic;
using Esos.Web.Infrastructure.DTO;

namespace Esos.Web.Infrastructure.Services.Interfaces
{
    public interface ILecturerService
    {
         IEnumerable<Activity> GetLecturerActivities(int lecturerId);

         IEnumerable<User> GetActivityStudents(int activityId);

         void AddMark(int studentId,int activityId, float value);

         void EditMark(int markId, float value);
    }
}