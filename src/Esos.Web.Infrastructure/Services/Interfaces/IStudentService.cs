using System.Collections.Generic;
using Esos.Web.Infrastructure.DTO;

namespace Esos.Web.Infrastructure.Services.Interfaces
{
    public interface IStudentService
    {
         void AcceptMark(int markId);

         void AddStudentActivity(int studentId, int activityId);

         void ComplainMark(int markId);

         void DeleteStudnetActivity(int studentId, int activityId);

         IEnumerable<Activity> GetAvailableActivities(int faluctyId);

         IEnumerable<Activity> GetStudentActivities(int studentId);

         IEnumerable<Mark> GetStudentMarks(int studentId);
    }
}