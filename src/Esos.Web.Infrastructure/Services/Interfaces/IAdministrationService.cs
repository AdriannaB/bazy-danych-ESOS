using System.Collections.Generic;
using Esos.Web.Infrastructure.DTO;

namespace Esos.Web.Infrastructure.Services.Interfaces
{
    public interface IAdministrationService
    {
         IEnumerable<User> GetUsers();

         void AddUser(User user);

         void EditUser(User user);

         void AddStudentActivity(int studentId, int activityId);

         void DeleteStudentActivity(int studentId, int activityId);

         IEnumerable<User> GetActivityStudents(int activityId);

         IEnumerable<Activity> GetActivities();

         void AddActivity(Activity activity);
    }
}