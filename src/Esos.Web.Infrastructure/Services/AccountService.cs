using System;
using System.Data;
using System.Linq;
using Dapper;
using Esos.Web.Core.DataAccess.Interfaces;
using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Services.Interfaces;

namespace Esos.Web.Infrastructure.Services
{
    public class AccountService : IAccountService
    {
        private IDatabase _database;
        public AccountService(IDatabase database)
        {
            _database = database;
        }
        public User LogIn(string login, string password)
        {
            User result = null;
            try
            {
                using(var connection = _database.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@login",login);
                    parameters.Add("@password",password);
                    result = connection.Query<User>(
                        "[dbo].[LogIn]",
                        parameters,
                        commandType:CommandType.StoredProcedure)
                        .AsList()
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}