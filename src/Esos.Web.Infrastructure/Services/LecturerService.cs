using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Esos.Web.Core.DataAccess.Interfaces;
using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Services.Interfaces;

namespace Esos.Web.Infrastructure.Services
{
    public class LecturerService : ILecturerService
    {
        private IDatabase _database;
        public LecturerService(IDatabase database)
        {
            this._database = database;
        }

        public void AddMark(int studentId, int activityId, float value)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@activityId",activityId);
                parameters.Add("@userId",studentId);
                parameters.Add("@value",value);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute("[dbo].[AddMark]",parameters,commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditMark(int markId, float value)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@markId",markId);
                parameters.Add("@value",value);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute("[dbo].[EditMark]",parameters,commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<User> GetActivityStudents(int activityId)
        {
            try
            {
                IEnumerable<User> result = null;
                var parameters = new DynamicParameters();
                parameters.Add("@activityId",activityId);
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<User>("EXECUTE [dbo].[GetActivityStudents]",parameters,commandType:CommandType.StoredProcedure);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Activity> GetLecturerActivities(int lecturerId)
        {
            try
            {
                IEnumerable<Activity> result = null;
                var parameters = new DynamicParameters();
                parameters.Add("@lecturerId",lecturerId);
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<Activity,Subject,Room,Faculty,Activity>("[dbo].[GetLecturerActivities]",
                    (activity, subject, room,faculty) => {activity.Room = room; activity.Subject=subject; activity.Faculty = faculty; return activity;},
                    parameters,
                    splitOn:"ID",
                    commandType:CommandType.StoredProcedure).Distinct().ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}