using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Esos.Web.Core.DataAccess.Interfaces;
using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Services.Interfaces;

namespace Esos.Web.Infrastructure.Services
{
    public class StudentService : IStudentService
    {
        private IDatabase _database;
        public StudentService(IDatabase database)
        {
            this._database = database;
        }

        public void AcceptMark(int markId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@markId", markId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute("[dbo].[AcceptMark]",parameters,commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddStudentActivity(int studentId, int activityId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId",studentId);
                parameters.Add("@activityId",activityId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[AddStudentActivity]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ComplainMark(int markId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@markId", markId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute("[dbo].[ComplainMark]",parameters,commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteStudnetActivity(int studentId, int activityId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId",studentId);
                parameters.Add("@activityId",activityId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[DeleteStudentActivity]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Activity> GetAvailableActivities(int faluctyId)
        {
            try
            {
                IEnumerable<Activity> result = null;
                var parameters = new DynamicParameters();
                parameters.Add("@facultyId",faluctyId);
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<Activity>("dbo].[GetAvailableActivities]",parameters,commandType:CommandType.StoredProcedure);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Activity> GetStudentActivities(int studentId)
        {
            try
            {
                IEnumerable<Activity> result = null;
                var parameters = new DynamicParameters();
                parameters.Add("@studentId",studentId);
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<Activity,Subject,Room,Faculty,Activity>("[dbo].[GetStudentActivities]",
                    (activity, subject, room,faculty) => {activity.Room = room; activity.Subject=subject; activity.Faculty = faculty; return activity;},
                    parameters,
                    splitOn:"ID",
                    commandType:CommandType.StoredProcedure).Distinct().ToList();

                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Mark> GetStudentMarks(int studentId)
        {
            try
            {
                IEnumerable<Mark> result = null;
                var parameters = new DynamicParameters();
                parameters.Add("@studentId",studentId);
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<Mark,Activity,Subject, Mark>("[dbo].[GetStudentMarks]",
                    (mark, activity, subject) => {activity.Subject=subject; mark.Activity = activity; return mark;},
                    parameters,
                    splitOn:"ID",
                    commandType:CommandType.StoredProcedure).Distinct().ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}