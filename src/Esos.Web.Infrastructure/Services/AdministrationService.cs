using Dapper;
using System.Collections.Generic;
using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Services.Interfaces;
using System.Data.SqlClient;
using Esos.Web.Core.DataAccess.Interfaces;
using System;
using System.Data;

namespace Esos.Web.Infrastructure.Services
{
    public class AdministrationService : IAdministrationService
    {
        private IDatabase _database;
        public AdministrationService(IDatabase database)
        {
            this._database = database;
        }

        public void AddActivity(Activity activity)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@lecturer_id",activity.Lecturer.ID);
                parameters.Add("@subject_id",activity.Subject.ID);
                parameters.Add("@room_id",activity.Room);
                parameters.Add("@type_id",activity.Type);
                parameters.Add("@term",activity.Term);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[AddActivity]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddStudentActivity(int studentId, int activityId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId",studentId);
                parameters.Add("@activityId",activityId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[AddStudentActivity]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
            
                throw ex;
            }
        }

        public void AddUser(User user)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@login",user.Login);
                parameters.Add("@password",user.Password);
                parameters.Add("@firstName",user.FirstName);
                parameters.Add("@secondName",user.SecondName);
                parameters.Add("@lastName",user.LastName);
                parameters.Add("@PESEL",user.PESEL);
                parameters.Add("@Role_id",user.Role);
                parameters.Add("@indeks",user.Indeks);
                parameters.Add("@email",user.Email);
                parameters.Add("@phoneNumber",user.PhoneNumber);
                parameters.Add("@dateOfBirth",user.DateOfBirth);
                parameters.Add("@score",user.Score);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[AddUser]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteStudentActivity(int studentId, int activityId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId",studentId);
                parameters.Add("@activityId",activityId);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[DeleteStudentActivity]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditUser(User user)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@userId",user.ID);
                parameters.Add("@firstName",user.FirstName);
                parameters.Add("@secondName",user.SecondName);
                parameters.Add("@lastName",user.LastName);
                parameters.Add("@PESEL",user.PESEL);
                parameters.Add("@Role_id",user.Role);
                parameters.Add("@indeks",user.Indeks);
                parameters.Add("@email",user.Email);
                parameters.Add("@phoneNumber",user.PhoneNumber);
                parameters.Add("@dateOfBirth",user.DateOfBirth);
                parameters.Add("@score",user.Score);
                using(var connection = _database.CreateConnection())
                {
                    connection.Execute(
                        "[dbo].[EditUser]",
                        parameters,
                        commandType:CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Activity> GetActivities()
        {
            IEnumerable<Activity>  result = new List<Activity>();
            try
            {
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<Activity>("EXECUTE [dbo].[GetActivities]");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IEnumerable<User> GetActivityStudents(int activityId)
        {
            IEnumerable<User>  result = new List<User>();
            try
            {
                using(var connection = _database.CreateConnection())
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@activityId",activityId);
                    result = connection.Query<User>("[dbo].[GetActivityStudents]",parameters, commandType: CommandType.StoredProcedure);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public IEnumerable<User> GetUsers()
        {
            IEnumerable<User>  result = new List<User>();
            try
            {
                using(var connection = _database.CreateConnection())
                {
                    result = connection.Query<User>("EXECUTE [dbo].[GetUsers]");
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}