namespace Esos.Web.Infrastructure.Enums
{
    public enum ActivityTypeEnum
    {
        None = 0,
        Lecture = 1,
        Exercises = 2,
        Laboratories = 3,
        Seminar = 4
    }
}