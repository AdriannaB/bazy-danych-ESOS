namespace Esos.Web.Infrastructure.Enums
{
    public enum RoleEnum
    {
        None = 0,
        Student = 1,
        Lecturer = 2,
        Administration = 3
    }
}