using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class Room : AbstractEntity
    {
        public Building Building { get; set; }
        public int StudentsLimit { get; set; }
        public int RoomNumber { get; set; }
        public int FloorNumber { get; set; }
    }
}