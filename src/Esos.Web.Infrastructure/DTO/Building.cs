using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class Building : AbstractEntity
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        
    }
}