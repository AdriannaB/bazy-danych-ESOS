using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO {
    public class Mark : AbstractEntity {
        public Activity Activity { get; set; }
        public User User { get; set; }
        public float Value { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsComplained { get; set; }
        
    }
}