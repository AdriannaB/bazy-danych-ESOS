namespace Esos.Web.Infrastructure.DTO.Abstract {
    public abstract class AbstractEntity 
    {
        public int ID { get; set; }
    }
}