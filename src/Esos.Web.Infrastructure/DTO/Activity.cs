using System;
using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class Activity : AbstractEntity
    {
        public User Lecturer { get; set; }
        public Subject Subject { get; set; }
        public Room Room { get; set; }
        public int Type { get; set; }
        public DateTime Term { get; set; }
        public Faculty Faculty {get;set;}
    }
}