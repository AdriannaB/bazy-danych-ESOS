using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class Faculty : AbstractEntity
    {
        public string Name { get; set; }
        public int Threshold { get; set; }
    }
}