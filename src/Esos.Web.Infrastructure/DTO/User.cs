using System;
using System.Collections.Generic;
using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class User : AbstractEntity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string PESEL { get; set; }
        public int Role { get; set; }
        public string Indeks { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Score { get; set; }
        public IList<Activity> Activities { get; set; }
        
        
    }
}