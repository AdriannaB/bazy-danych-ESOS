using Esos.Web.Infrastructure.DTO.Abstract;

namespace Esos.Web.Infrastructure.DTO
{
    public class Subject : AbstractEntity
    {
        public string Name { get; set; }
        public int ECTS { get; set; }
        public int Semester { get; set; }
        public Faculty Faculty { get; set; }
    }
}