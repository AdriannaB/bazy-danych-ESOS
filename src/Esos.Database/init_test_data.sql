USE ESOS
GO
PRINT N'Creating test data';
GO
SET NOCOUNT ON;
GO

SET IDENTITY_INSERT BUILDINGS ON
GO

INSERT INTO BUILDINGS
(ID,Name, Street,Number,PostCode,City)
VALUES
(1,'C-1','Kosciuszki',1,'34-230','Wroclaw'),
(2,'C-2','Kosciuszki',2,'34-230','Wroclaw'),
(3,'C-2','Kosciuszki',3,'34-230','Wroclaw');
GO

SET IDENTITY_INSERT BUILDINGS OFF
GO

SET IDENTITY_INSERT ROOMS ON
GO

INSERT INTO ROOMS
(ID,Building_id,StudentsLimit,RoomNumber,FloorNumber)
VALUES
(1,1,200,1,0),
(2,2,15,1,0),
(3,3,20,1,0);
GO

SET IDENTITY_INSERT ROOMS OFF
GO

SET IDENTITY_INSERT FACULTIES ON
GO

INSERT INTO FACULTIES
(ID,Name,Threshold)
VALUES
(1,'Infa',200),
(2,'AiR',50),
(3,'Matma',300);
GO

SET IDENTITY_INSERT FACULTIES OFF
GO

SET IDENTITY_INSERT SUBJECTS ON
GO

INSERT INTO SUBJECTS
(ID,Name,ECTS,Semester,Faculty_id)
VALUES
(1,'Programowanie',5,1,1),
(2,'Robotyka',4,1,1),
(3,'Analiza matematyczna',8,1,1),
(4,'Algebra',6,1,1),
(5,'Miernictwo',2,1,1),
(6,'Etyka',1,1,1),
(7,'Filozofia',2,1,1),
(8,'Statystyka',2,1,1);
GO

SET IDENTITY_INSERT SUBJECTS OFF
GO

SET IDENTITY_INSERT USERS ON
GO

INSERT INTO USERS
(ID,Login,Password,FirstName,SecondName,LastName,PESEL,Role_id,Indeks,Email,PhoneNumber,DateOfBirth,Score)
VALUES
(1,'krystiankolad','kolad','Krystian',NULL,'Kolad','7474637',1,'123456','krystian@kolad.pl','2314','1996-10-10',250),
(2,'annazajac','zajac','Anna',NULL,'Zajac','3213133',2,'123456','anna@zajac.pl','3213123','1942-10-10',NULL),
(3,'adriannabisikiewicz','bisikiewicz','Adrianna',NULL,'Bisikiewicz','2134512',3,'123456','adrianna@bisikiewicz.pl','32113','1985-10-10',NULL),
(4,'romangaj','gaj','Roman',NULL,'Gaj','1111111',3,'123456','roman@gaj.pl','321113','1985-10-10',NULL),
(5,'ewazimna','gaj','Ewa',NULL,'Zimna','2222222',3,'123456','ewa@zimna.pl','32113','1985-10-10',NULL),
(6,'nataliakijek','gaj','Natalia',NULL,'Kijek','3333333',3,'123456','natalia@kijek.pl','312113','1985-10-10',NULL),
(7,'monikadrzewo','gaj','Monika',NULL,'Drzewo','4444444',3,'123456','monika@drzewo.pl','32113','1985-10-10',NULL),
(8,'darekbrzoza','gaj','Darek',NULL,'Brzoza','5555555',3,'123456','darek@brzoza.pl','332113','1985-10-10',NULL),
(9,'sebastianarab','gaj','Sebastian',NULL,'Arab','6666666',3,'123456','sebastian@brzoza.pl','324113','1985-10-10',NULL),
(10,'kamilczekoalda','gaj','Kamil',NULL,'Czekolada','7777777',3,'123456','kamil@czekolada.pl','321163','1985-10-10',NULL),
(11,'jolantakiel','gaj','Jolanta',NULL,'Kiel','8888888',3,'123456','jolanta@kielpl','321173','1985-10-10',NULL),
(12,'grzegorzsamotnik','gaj','Grzegorz',NULL,'Samotnik','9999999',3,'123456','grzegorz@samotnik.pl','328113','1985-10-10',NULL),
(13,'jurekbukowski','gaj','Jurek',NULL,'Bukowski','0000000',3,'123456','jurek@bukowski.pl','321013','1985-10-10',NULL)
GO

SET IDENTITY_INSERT USERS OFF
GO

SET IDENTITY_INSERT ACTIVITIES ON
GO

INSERT INTO ACTIVITIES
(ID,Lecturer_id,Subject_id,Room_id,Type_id,Faculty_id,Term)
VALUES
(1,2,1,1,1,1,'2017/10/10'),
(2,2,2,2,2,2,'2017/10/11'),
(3,2,3,3,3,3,'2017/10/12'),
(4,2,4,1,1,1,'2017/10/13'),
(5,2,5,2,2,2,'2017/10/14'),
(6,2,6,3,3,3,'2017/10/15'),
(7,2,7,1,1,1,'2017/10/16'),
(8,2,8,2,2,2,'2017/10/17');
GO

SET IDENTITY_INSERT ACTIVITIES OFF
GO

SET IDENTITY_INSERT USERACTIVITY ON
GO

INSERT INTO USERACTIVITY
(ID,User_id,Activity_id)
VALUES
(1,1,1),
(2,1,2),
(3,1,3),
(4,1,4),
(5,1,5),
(6,1,6),
(7,1,7),
(8,1,8);
GO

SET IDENTITY_INSERT USERACTIVITY OFF
GO

SET IDENTITY_INSERT MARKS ON
GO

INSERT INTO MARKS
(ID,Activity_id,User_id,Value,IsAccepted)
VALUES
(1,1,1,5,0),
(2,2,1,4,0),
(3,3,1,3,0);
GO

SET IDENTITY_INSERT MARKS OFF
GO

SET NOCOUNT OFF;
PRINT N'Test data created';
GO