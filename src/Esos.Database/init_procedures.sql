USE ESOS
GO
PRINT N'Creating database procedures'
GO
-- LogIn(name,password)
CREATE PROCEDURE [dbo].[LogIn](
    @login VARCHAR(100),
    @password VARCHAR(256)
)
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT 
            ID,
            FirstName,
            SecondName,
            LastName,
            PESEL,
            Role_id AS Role,
            Indeks,
            Email,
            PhoneNumber,
            DateOfBirth,
            Score
        FROM 
            USERS
        WHERE
            Login like @login
            AND
            Password like @password;
    END
GO
GRANT EXECUTE ON [dbo].[LogIn] TO EsosApp
GO

-- GetStudentMarks
CREATE PROCEDURE [dbo].[GetStudentMarks](
    @studentId INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT
             *
        FROM 
            MARKS m
            JOIN USERS u ON m.User_ID = u.ID
            jOIN ACTIVITIES a ON m.Activity_id = a.ID
            JOIN USERS lec ON a.Lecturer_id = lec.ID
            JOIN SUBJECTS sub ON a.Subject_id = sub.ID
        WHERE
            u.ID = @studentId
    END
GO
GRANT EXECUTE ON [dbo].[GetStudentMarks] TO EsosApp
GO
-- AddMark
CREATE PROCEDURE [dbo].[AddMark](
    @activityId INT,
    @userId INT,
    @value float(2)
)
AS
    BEGIN
        SET NOCOUNT ON;  
        INSERT INTO MARKS
        (Activity_id,User_id,Value,IsAccepted)
        VALUES
        (@activityId,@userId,@value,0);
    END
GO
GRANT EXECUTE ON [dbo].[AddMark] TO EsosApp
GO

-- AcceptMark
CREATE PROCEDURE [dbo].[AcceptMark](
    @markId INT
)
AS
    BEGIN
        SET NOCOUNT ON; 
        UPDATE MARKS
        SET IsAccepted = 1,  IsComplained = 0
        WHERE ID = @markId;
    END
GO
GRANT EXECUTE ON [dbo].[AcceptMark] TO EsosApp
GO

-- EditMark
CREATE PROCEDURE [dbo].[EditMark](
    @markId INT,
    @value float(2)
)
AS
    BEGIN
        SET NOCOUNT ON; 
        UPDATE MARKS
        SET IsAccepted = 1, IsComplained = 0, Value = @value
        WHERE ID = @markId;
    END
GO
GRANT EXECUTE ON [dbo].[EditMark] TO EsosApp
GO

-- GetStudentActivities
CREATE PROCEDURE [dbo].[GetStudentActivities](
    @studentId INT
)
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT 
            a.*,
            s.*,
            r.*,
            f.*
        FROM ACTIVITIES a 
        JOIN SUBJECTS s ON s.ID = a.Subject_id
        JOIN ROOMS r on r.ID = a.Room_id
        JOIN FACULTIES f on f.ID = a.Faculty_Id
        JOIN USERACTIVITY ua ON ua.Activity_id = a.ID
        JOIN USERs u ON u.ID = ua.User_id 
        WHERE 
            u.ID = @studentId;
    END
GO
GRANT EXECUTE ON [dbo].[GetStudentActivities] TO EsosApp
GO

-- AddUser()
CREATE PROCEDURE [dbo].[AddUser](
    @login VARCHAR(100),
    @password varchar(256),
    @firstName VARCHAR(30),
    @secondName VARCHAR(30),
    @lastName VARCHAR(50),
    @PESEL VARCHAR(11),
    @Role_id INT,
    @indeks VARCHAR(6),
    @email VARCHAR(300),
    @phoneNumber VARCHAR(13),
    @dateOfBirth DATE,
    @score INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        INSERT INTO USERS
        (Login,
        Password,
        FirstName,
        SecondName,
        LastName,
        PESEL,
        Role_id,
        Indeks,
        Email,
        PhoneNumber,
        DateOfBirth,
        Score)
        VALUES
        (@login,
        @password,
        @firstName,
        @secondName,
        @lastName,
        @PESEL,
        @role_id,
        @indeks,
        @email,
        @phoneNumber,
        @dateOfBirth,
        @score)
    END
GO
GRANT EXECUTE ON [dbo].[AddUser] TO EsosApp
GO

-- GetUsers()
CREATE PROCEDURE [dbo].[GetUsers]
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            ID,
            FirstName,
            SecondName,
            LastName,
            PESEL,
            Role_id AS Role,
            Indeks,
            Email,
            PhoneNumber,
            DateOfBirth,
            Score
        FROM
            USERS
    END
GO
GRANT EXECUTE ON [dbo].[GetUsers] TO EsosApp
GO

-- EditUser
CREATE PROCEDURE [dbo].[EditUser](
    @userId INT,
    @firstName VARCHAR(30),
    @secondName VARCHAR(30),
    @lastName VARCHAR(50),
    @PESEL VARCHAR(11),
    @role_id INT,
    @indeks VARCHAR(6),
    @email VARCHAR(300),
    @phoneNumber VARCHAR(13),
    @dateOfBirth DATE,
    @score INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        UPDATE USERS
        SET 
            FirstName = @firstName,
            SecondName = @secondName,
            LastName = @lastName,
            PESEL = @PESEL,
            Role_id = @role_id,
            Indeks = @indeks,
            Email = @email,
            PhoneNumber = @phoneNumber,
            DateOfBirth = @dateOfBirth,
            Score = @score
        WHERE
            ID = @userId
    END
GO
GRANT EXECUTE ON [dbo].[EditUser] TO EsosApp
GO

-- DeleteStudentActivity
CREATE PROCEDURE [dbo].[DeleteStudentActivity](
    @userId INT,
    @activityId INT
)
AS
    BEGIN
        SET NOCOUNT ON;
        DELETE FROM USERACTIVITY
        WHERE 
            User_id = @userId 
            AND 
            Activity_id = @activityId
    END
GO
GRANT EXECUTE ON [dbo].[DeleteStudentActivity] TO EsosApp
GO

-- AddStudentActivity
CREATE PROCEDURE [dbo].[AddStudentActivity](
    @userId INT,
    @activityId INT
)
AS
    BEGIN
        SET NOCOUNT ON;
        INSERT INTO USERACTIVITY
        (User_id,Activity_id)
        VALUES
        (@userId,@activityId)
    END
GO
GRANT EXECUTE ON [dbo].[AddStudentActivity] TO EsosApp
GO

-- GetActivityStudents
CREATE PROCEDURE [dbo].[GetActivityStudents](
    @activityId INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT 
            s.FirstName,
            s.LastName,
            s.Indeks
        FROM USERS s
            JOIN USERACTIVITY ua on s.ID = ua.User_Id
            JOIN ACTIVITIES a on ua.Activity_id = a.ID
        WHERE a.ID = @activityId; 
    END
GO
GRANT EXECUTE ON [dbo].[GetActivityStudents] TO EsosApp
GO

CREATE PROCEDURE [dbo].[GetActivities]
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT 
            *
        FROM ACTIVITIES a 
        JOIN SUBJECTS s ON s.ID = a.Subject_id
        JOIN ROOMS r on r.ID = a.Room_id
        JOIN FACULTIES f on f.ID = a.Faculty_Id
    END
GO
GRANT EXECUTE ON [dbo].[GetActivities] TO EsosApp
GO

CREATE PROCEDURE [dbo].[AddActivity](
    @lecturer_id INT,
    @subject_id INT,
    @room_id INT,
    @type_id INT,
    @term DATETIME2
)
AS
    BEGIN
        SET NOCOUNT ON;  
        INSERT INTO ACTIVITIES
            (Lecturer_id,
            Subject_id,
            Room_id,
            Type_id,
            Term)
            VALUES
            (@lecturer_id,
            @subject_id,
            @room_id,
            @type_id,
            @term)
    END
GO
GRANT EXECUTE ON [dbo].[AddActivity] TO EsosApp
GO


CREATE PROCEDURE [dbo].[GetLecturerActivities](
    @lecturerId INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT 
            *
        FROM ACTIVITIES a 
        JOIN SUBJECTS s ON s.ID = a.Subject_id
        JOIN ROOMS r on r.ID = a.Room_id
        JOIN FACULTIES f on f.ID = a.Faculty_Id
        WHERE 
            a.Lecturer_id = @lecturerId
    END
GO
GRANT EXECUTE ON [dbo].[GetLecturerActivities] TO EsosApp
GO

CREATE PROCEDURE [dbo].[ComplainMark](
    @markId INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        UPDATE MARKS
        SET IsComplained = 1, IsAccepted = 0
        WHERE ID = @markId
    END
GO
GRANT EXECUTE ON [dbo].[ComplainMark] TO EsosApp
GO

CREATE PROCEDURE [dbo].[GetAvailableActivities](
    @faluctyId INT
)
AS
    BEGIN
        SET NOCOUNT ON;  
        SELECT 
            *
        FROM ACTIVITIES a 
        JOIN SUBJECTS s ON s.ID = a.Subject_id
        JOIN ROOMS r on r.ID = a.Room_id
        JOIN FACULTIES f on f.ID = a.Faculty_Id
        WHERE 
            a.Faculty_Id = @faluctyId
    END
GO
GRANT EXECUTE ON [dbo].[GetAvailableActivities] TO EsosApp
GO
PRINT N'Creating database procedures done'