-- Create schemas
CREATE DATABASE ESOS;
GO
USE ESOS;
GO
PRINT N'Creating database tables'
-- Create tables
IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'USERS'))
BEGIN
  PRINT N'Creating table Users'
  CREATE TABLE USERS
  (
    ID INT NOT NULL UNIQUE IDENTITY(1,1),
    Login VARCHAR(100) NOT NULL,
    Password varchar(256) NOT NULL,
    FirstName VARCHAR(30) NOT NULL,
    SecondName VARCHAR(30),
    LastName VARCHAR(50) NOT NULL,
    PESEL VARCHAR(11) NOT NULL,
    Role_id INT NOT NULL,
    Indeks VARCHAR(6),
    Email VARCHAR(300),
    PhoneNumber VARCHAR(13),
    DateOfBirth DATE NOT NULL,
    Score INT,
    Created DATETIME2,
    Updated DATETIME2,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ROLES'))
BEGIN
  PRINT N'Creating table Roles'
  CREATE TABLE ROLES
  (
    ID INT NOT NULL IDENTITY(1,1),
    Name VARCHAR(50),
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ROOMS'))
BEGIN
  PRINT N'Creating table Rooms'
  CREATE TABLE ROOMS
  (
    ID INT NOT NULL IDENTITY(1,1),
    Building_id INT,
    StudentsLimit INT,
    RoomNumber INT,
    FloorNumber INT,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'BUILDINGS'))
BEGIN
  PRINT N'Creating table Buildings'
  CREATE TABLE BUILDINGS
  (
    ID INT NOT NULL IDENTITY(1,1),
    Name VARCHAR(50),
    Street VARCHAR(50),
    Number INT,
    PostCode VARCHAR(6),
    City VARCHAR(50),
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'SUBJECTS'))
BEGIN
  PRINT N'Creating table Subjects'
  CREATE TABLE SUBJECTS
  (
    ID INT NOT NULL IDENTITY(1,1),
    Name VARCHAR(50),
    ECTS INT,
    Semester INT,
    Faculty_id INT,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MARKS'))
BEGIN
  PRINT N'Creating table Marks'
  CREATE TABLE MARKS
  (
    ID INT NOT NULL IDENTITY(1,1),
    Activity_id INT,
    User_id INT,
    Value FLOAT(2),
    IsAccepted BIT,
    IsComplained BIT,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ACTIVITIES'))
BEGIN
  PRINT N'Creating table Activities'
  CREATE TABLE ACTIVITIES
  (
    ID INT NOT NULL IDENTITY(1,1),
    Lecturer_id INT,
    Subject_id INT,
    Room_id INT,
    Type_id INT,
    Faculty_id INT,
    Term DATETIME2,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'USERACTIVITY'))
BEGIN
  PRINT N'Creating table UserActivity'
  CREATE TABLE USERACTIVITY
  (
    ID INT NOT NULL IDENTITY(1,1),
    User_id INT,
    Activity_id INT,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ACTIVITYTYPE'))
BEGIN
  PRINT N'Creating table ActivityType'
  CREATE TABLE ACTIVITYTYPE
  (
    ID INT NOT NULL IDENTITY(1,1),
    Name VARCHAR(100),
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;

IF (NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'FACULTIES'))
BEGIN
  PRINT N'Creating table Faculties'
  CREATE TABLE FACULTIES
  (
    ID INT NOT NULL IDENTITY(1,1),
    Name VARCHAR(100),
    Threshold INT NOT NULL,
    Created Date,
    Updated Date,
    PRIMARY KEY(ID)
  )
END;
GO


-- Create FKs
ALTER TABLE USERS
    ADD    FOREIGN KEY (Role_id)
    REFERENCES ROLES(ID)
;
    
ALTER TABLE ROOMS
    ADD    FOREIGN KEY (Building_id)
    REFERENCES BUILDINGS(ID)
;
    
ALTER TABLE SUBJECTS
    ADD    FOREIGN KEY (Faculty_id)
    REFERENCES FACULTIES(ID)
;
    
ALTER TABLE MARKS
    ADD    FOREIGN KEY (Activity_id)
    REFERENCES ACTIVITIES(ID)
;
    
ALTER TABLE MARKS
    ADD    FOREIGN KEY (User_id)
    REFERENCES USERS(ID)
;
    
ALTER TABLE ACTIVITIES
    ADD    FOREIGN KEY (Lecturer_id)
    REFERENCES USERS(ID)
;
    
ALTER TABLE ACTIVITIES
    ADD    FOREIGN KEY (Subject_id)
    REFERENCES SUBJECTS(ID)
;
    
ALTER TABLE ACTIVITIES
    ADD    FOREIGN KEY (Room_id)
    REFERENCES ROOMS(ID)
;
    
ALTER TABLE USERACTIVITY
    ADD    FOREIGN KEY (User_id)
    REFERENCES USERS(ID)
;
    
ALTER TABLE ACTIVITIES
    ADD    FOREIGN KEY (Type_id)
    REFERENCES ACTIVITYTYPE(ID)
;

ALTER TABLE USERACTIVITY
    ADD    FOREIGN KEY (Activity_id)
    REFERENCES ACTIVITIES(ID)
;
GO
CREATE UNIQUE INDEX Users_index ON USERS (ID,FirstName, LastName, PESEL);
GO
CREATE UNIQUE INDEX Marks_index ON Marks (ID,User_id, Activity_id);
GO

CREATE TRIGGER UserSInsert ON Users
FOR INSERT
AS
  BEGIN
    UPDATE Users
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Users.ID
  END
GO
CREATE TRIGGER UserSUpdate ON Users
FOR UPDATE
AS
  BEGIN
    UPDATE Users
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Users.ID
  END
GO

CREATE TRIGGER RolesInsert ON Roles
FOR INSERT
AS
  BEGIN
    UPDATE Roles
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Roles.ID
  END
GO
CREATE TRIGGER RolesUpdate ON Roles
FOR UPDATE
AS
  BEGIN
    UPDATE Roles
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Roles.ID
  END
GO

CREATE TRIGGER RoomsInsert ON Rooms
FOR INSERT
AS
  BEGIN
    UPDATE Rooms
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Rooms.ID
  END
GO
CREATE TRIGGER RoomsUpdate ON Rooms
FOR UPDATE
AS
  BEGIN
    UPDATE Rooms
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Rooms.ID
  END
GO

CREATE TRIGGER BuildingsInsert ON Buildings
FOR INSERT
AS
  BEGIN
    UPDATE Buildings
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Buildings.ID
  END
GO
CREATE TRIGGER BuildingsUpdate ON Buildings
FOR UPDATE
AS
  BEGIN
    UPDATE Buildings
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Buildings.ID
  END
GO

CREATE TRIGGER SubjectsInsert ON Subjects
FOR INSERT
AS
  BEGIN
    UPDATE Subjects
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Subjects.ID
  END
GO
CREATE TRIGGER SubjectsUpdate ON Subjects
FOR UPDATE
AS
  BEGIN
    UPDATE Subjects
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Subjects.ID
  END
GO

CREATE TRIGGER MarksInsert ON Marks
FOR INSERT
AS
  BEGIN
    UPDATE Marks
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Marks.ID
  END
GO
CREATE TRIGGER MarksUpdate ON Marks
FOR UPDATE
AS
  BEGIN
    UPDATE Marks
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Marks.ID
  END
GO

CREATE TRIGGER ActivitiesInsert ON Activities
FOR INSERT
AS
  BEGIN
    UPDATE Activities
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Activities.ID
  END
GO
CREATE TRIGGER ActivitiesUpdate ON Activities
FOR UPDATE
AS
  BEGIN
    UPDATE Activities
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Activities.ID
  END
GO

CREATE TRIGGER UseractivityInsert ON Useractivity
FOR INSERT
AS
  BEGIN
    UPDATE Useractivity
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Useractivity.ID
  END
GO
CREATE TRIGGER UseractivityUpdate ON Useractivity
FOR UPDATE
AS
  BEGIN
    UPDATE Useractivity
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Useractivity.ID
  END
GO

CREATE TRIGGER ActivitytypeInsert ON Activitytype
FOR INSERT
AS
  BEGIN
    UPDATE Activitytype
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Activitytype.ID
  END
GO
CREATE TRIGGER ActivitytypeUpdate ON Activitytype
FOR UPDATE
AS
  BEGIN
    UPDATE Activitytype
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Activitytype.ID
  END
GO

CREATE TRIGGER FacultiesInsert ON Faculties
FOR INSERT
AS
  BEGIN
    UPDATE Faculties
    SET Created = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Faculties.ID
  END
GO
CREATE TRIGGER FacultiesUpdate ON Faculties
FOR UPDATE
AS
  BEGIN
    UPDATE Faculties
    SET Updated = CURRENT_TIMESTAMP
    FROM inserted
    WHERE inserted.ID = Faculties.ID
  END
GO


SET NOCOUNT ON
GO
SET IDENTITY_INSERT ROLES ON
GO
INSERT INTO ROLES
(ID,NAME)
VALUES
(1,'Student'),
(2,'Lecturer'),
(3,'Administration')
GO

SET IDENTITY_INSERT ROLES OFF
GO

SET IDENTITY_INSERT ACTIVITYTYPE ON
GO

INSERT INTO ACTIVITYTYPE
(ID,NAME)
VALUES
(1,'Lecture'),
(2,'Exercises'),
(3,'Laboratories'),
(4,'Seminar')
GO

SET IDENTITY_INSERT ACTIVITYTYPE OFF
GO

SET NOCOUNT OFF;
GO
PRINT N'Creating database tables done'
PRINT N'Creating app user'
CREATE LOGIN EsosApp WITH PASSWORD='EsosApp123!.'
GO
CREATE USER EsosApp FOR LOGIN EsosApp
GO
PRINT N'User Created'

-- Create Indexes