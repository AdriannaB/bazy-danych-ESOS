namespace Esos.Web.Core.Settings.Interfaces
{
    public interface IDBSettings
    {
         string GetConnectionString();
         string GetTestConnectionString();
         string GetProductionConnectionString();
    }
}