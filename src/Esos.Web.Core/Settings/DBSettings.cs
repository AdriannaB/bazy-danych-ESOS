using Esos.Web.Core.Settings.Interfaces;

namespace Esos.Web.Core.Settings 
{
    public class DBSettings : IDBSettings
    {
        public string GetConnectionString()
        {
            return "Server=localhost;User Id=EsosApp; Password=EsosApp123!.; Database=ESOS";
        }

        public string GetProductionConnectionString()
        {
            return "Server=mssql;User Id=EsosApp; Password=EsosApp123!.; Database=ESOS";
        }

        public string GetTestConnectionString()
        {
            return "Server=localhost;User Id=EsosApp; Password=EsosApp123!.; Database=ESOS";
        }
    }
}