using System.Data;
using System.Data.SqlClient;
using Esos.Web.Core.DataAccess.Interfaces;
using Esos.Web.Core.Settings.Interfaces;

namespace Esos.Web.Core.DataAccess
{
    public class Database : IDatabase
    {
        private IDBSettings _databaseSettings;
        public Database(IDBSettings databaseSettings)
        {
            _databaseSettings = databaseSettings;
        }
        
        public IDbConnection CreateConnection()
        {
            return new SqlConnection(_databaseSettings.GetConnectionString());
        }
    }
}