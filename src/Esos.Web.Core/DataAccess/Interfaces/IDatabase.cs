using System.Data;
using System.Data.SqlClient;

namespace Esos.Web.Core.DataAccess.Interfaces
{
    public interface IDatabase
    {
         IDbConnection CreateConnection();
    }
}