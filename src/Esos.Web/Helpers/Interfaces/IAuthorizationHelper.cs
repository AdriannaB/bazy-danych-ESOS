using Esos.Web.Requests.Abstract;

namespace Esos.Web.Helpers.Interfaces
{
    public interface IAuthorizationHelper
    {
         bool Authorize(AbstractRequest request, int role);
    }
}