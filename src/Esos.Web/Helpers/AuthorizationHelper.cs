using Esos.Web.Helpers.Interfaces;
using Esos.Web.Requests.Abstract;

namespace Esos.Web.Helpers
{
    public class AuthorizationHelper : IAuthorizationHelper
    {
        public bool Authorize(AbstractRequest request, int role)
        {
            return request.Requester.Role == role;
        }
    }
}