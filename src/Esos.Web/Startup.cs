using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Esos.Web.Core.DataAccess;
using Esos.Web.Core.DataAccess.Interfaces;
using Esos.Web.Core.Settings;
using Esos.Web.Core.Settings.Interfaces;
using Esos.Web.Helpers;
using Esos.Web.Helpers.Interfaces;
using Esos.Web.Infrastructure.Services;
using Esos.Web.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Microsoft.Extensions.Logging;

namespace Esos_Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            services.AddTransient<IDBSettings,DBSettings>();
            services.AddTransient<IDatabase,Database>();
            services.AddTransient<IAccountService,AccountService>();
            services.AddTransient<IAdministrationService,AdministrationService>();
            services.AddTransient<IStudentService,StudentService>();
            services.AddTransient<ILecturerService,LecturerService>();
            services.AddTransient<IAuthorizationHelper,AuthorizationHelper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });

            loggerFactory.AddDebug();
            loggerFactory.AddConsole();
        }
    }
}
