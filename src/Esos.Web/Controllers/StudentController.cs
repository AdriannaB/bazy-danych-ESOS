using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Esos.Web.Infrastructure.Services.Interfaces;
using Esos.Web.Helpers.Interfaces;
using Esos.Web.Infrastructure.Enums;
using Esos.Web.Requests.Student;
using Esos.Web.Infrastructure.DTO;
using System.Net.Http;
using System.Net;

namespace Esos.Web.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : Controller
    {
        private int _role;
        private IAuthorizationHelper _authorizationHelper;
        private IStudentService _service;
        public StudentController(IStudentService service, IAuthorizationHelper authorizationHelper)
        {
            _service = service;
            _role = (int)RoleEnum.Student;
            _authorizationHelper = authorizationHelper;
        }

        [HttpPost("[action]")]
        public HttpResponseMessage AcceptMark([FromBody]AcceptMarkRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AcceptMark(request.MarkId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public HttpResponseMessage AddStudentActivity([FromBody]AddStudentActivityRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AddStudentActivity(request.StudentId,request.ActivityId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public HttpResponseMessage Complainmark([FromBody]ComplainMarkRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.ComplainMark(request.MarkId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public HttpResponseMessage DeleteStudnetActivity([FromBody]DeleteStudentActivityRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.DeleteStudnetActivity(request.StudentId,request.ActivityId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public IEnumerable<Activity> GetAvailableActivities([FromBody]GetAvailableActivitiesRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetAvailableActivities(request.FacultyId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public IEnumerable<Activity> GetStudentActivities([FromBody]GetStudentActivitiesRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
               return _service.GetStudentActivities(request.StudentId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
        
        [HttpPost("[action]")]
        public IEnumerable<Mark> GetStudentMarks([FromBody]GetStudentMarksRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetStudentMarks(request.StudentId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
    }
}