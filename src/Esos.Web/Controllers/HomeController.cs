using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Esos_Web.Controllers
{
    public class HomeController : Controller
    {
        private ILogger _logger;
        public HomeController(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("Home");
        }
        public IActionResult Index()
        {
            _logger.LogInformation("Strona uruchomiona");
            return View();
        }

        public IActionResult Error()
        {
            _logger.LogError("Wystąpił błąd aplikacji");
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
