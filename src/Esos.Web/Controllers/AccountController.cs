using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Services.Interfaces;
using Esos.Web.Requests.Account;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Esos.Web.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private IAccountService _service;
        private ILogger _logger;
        public AccountController(IAccountService service, ILoggerFactory loggerFactory)
        {
            _service = service;
            _logger = loggerFactory.CreateLogger("Account");
        }

        [HttpPost("[action]")]
        public User LogIn([FromBody]LoginRequest request)
        {
            _logger.LogInformation("Uytkownik się loguje");
            return _service.LogIn(request.Login, request.Password);
        }
    }
}