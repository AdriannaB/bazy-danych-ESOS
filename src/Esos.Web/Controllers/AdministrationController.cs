using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Esos.Web.Infrastructure.Services.Interfaces;
using Esos.Web.Infrastructure.Services;
using Esos.Web.Requests.Administration;
using Esos.Web.Infrastructure.DTO;
using Esos.Web.Infrastructure.Enums;
using Esos.Web.Helpers.Interfaces;
using Esos.Web.Requests.Abstract;
using System.Net.Http;
using System.Net;

namespace Esos.Web.Controllers
{
    [Route("api/[controller]")]
    public class AdministrationController : Controller
    {
        private int _role;
        private IAuthorizationHelper _authorizationHelper;
        private IAdministrationService _service;

        public AdministrationController(IAdministrationService service, IAuthorizationHelper authorizationHelper)
        {
            _service = service;
            _role = (int)RoleEnum.Administration;
            _authorizationHelper = authorizationHelper;
        }

        [HttpPost("[action]")]
        public IEnumerable<User> GetUsers([FromBody]GetUsersRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetUsers();
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage AddUser([FromBody]AddUserRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AddUser(request.User);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage EditUser([FromBody]EditUserRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.EditUser(request.User);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage AddStudentActivity([FromBody]AddStudentActivityRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AddStudentActivity(request.StudentId,request.ActivityId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage DeleteStudentActivity([FromBody]DeleteStudentActivityRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.DeleteStudentActivity(request.StudentId,request.ActivityId);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public IEnumerable<User> GetActivityStudents([FromBody]GetActivityStudentsRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetActivityStudents(request.ActivityId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public IEnumerable<Infrastructure.DTO.Activity> GetActivities([FromBody]GetActivitiesRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetActivities();
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage AddAvtivity([FromBody]AddActivityRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AddActivity(request.Activity);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
    }
}