using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Esos.Web.Infrastructure.Services.Interfaces;
using Esos.Web.Helpers.Interfaces;
using Esos.Web.Infrastructure.Enums;
using Esos.Web.Requests.Lecturer;
using System.Net.Http;
using System.Net;

namespace Esos.Web.Controllers
{
    [Route("api/[controller]")]
    public class LecturerController : Controller
    {
        private int _role;
        private IAuthorizationHelper _authorizationHelper;
        private ILecturerService _service;
        public LecturerController(ILecturerService service, IAuthorizationHelper authorizationHelper)
        {
            _service = service;
            _role = (int)RoleEnum.Lecturer;
            _authorizationHelper = authorizationHelper;
        }

        [HttpPost("[action]")]
        public HttpResponseMessage AddMark([FromBody]AddMarkRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.AddMark(request.StudnetId,request.ActivityId,request.Value);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public HttpResponseMessage EditMark([FromBody]EditMarkRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                _service.EditMark(request.MarkId,request.Value);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public IEnumerable<Infrastructure.DTO.User> GetActivityStudents([FromBody]GetActivityStudentsRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetActivityStudents(request.ActivityId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }

        [HttpPost("[action]")]
        public IEnumerable<Infrastructure.DTO.Activity> GetLecturerActivities([FromBody]GetLecturerActivitiesRequest request)
        {
            if(_authorizationHelper.Authorize(request,_role))
            {
                return _service.GetLecturerActivities(request.LecturerId);
            }
            else
            {
                throw new AccessViolationException();
            }
        }
    }
}