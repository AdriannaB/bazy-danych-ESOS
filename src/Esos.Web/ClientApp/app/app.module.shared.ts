import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { NotImplementedComponent } from './components/not-implemented-yet/not-implemented.component';
import { ListOfUsersComponent } from './components/list-of-users/list-of-users.component';

import { StoreModule } from '@ngrx/store';
import { loginReducer } from './store/user/login.reducer';
import { LoginService } from './components/services/login.service';
import { UserService } from './components/services/user-list.service';
import { AddUserService } from './components/services/add-user.service';
import { SubjectService } from './components/services/subject.service';
import { MarksService } from './components/services/marks.service';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StudentComponent } from './components/student/student.component';
import { SubjectForStudentComponent } from './components/subject-for-student/subject-for-student.component';
import { MarksForStudentComponent } from './components/marks-for-student/marks-for-student.component';
import { EditUserService } from './components/services/edit-user.service';
import { SubjectForLecturerComponent } from './components/subject-for-lecturer/subject-for-lecturer.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        LoginComponent,
        AdministratorComponent,
        NotImplementedComponent,
        ListOfUsersComponent,
        StudentComponent,
        SubjectForStudentComponent,
        MarksForStudentComponent,
        SubjectForLecturerComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        StoreModule.forRoot({loginReducer}), 
        StoreModule.forFeature('user',loginReducer),
        StoreDevtoolsModule.instrument({
            maxAge: 10
        }),
        RouterModule.forRoot([
            { path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'administrator', component: AdministratorComponent },
            { path: 'student', component: StudentComponent },
            { path: 'login', component: LoginComponent },
            { path: 'home', component: HomeComponent },
            { path: '404', component: NotImplementedComponent },
            { path: 'list-of-users', component: ListOfUsersComponent },      
            { path: 'marks-for-student', component: MarksForStudentComponent },
            { path: 'subject-for-student', component: SubjectForStudentComponent },
            { path: 'subject-for-lecturer', component: SubjectForLecturerComponent },
            { path: '**', redirectTo: 'login' },
        ])
    ],
    providers: [LoginService, UserService, AddUserService, SubjectService, MarksService, EditUserService ],
})
export class AppModuleShared {
}
    