import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginService } from '../services/login.service';
import { State } from "../../store/app.store";
import { OnChanges, OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { MarksService } from '../services/marks.service';


@Component({
    selector: 'marks-for-student',
    templateUrl: './marks-for-student.component.html',
    styleUrls: ['./marks-for-student.component.css']
})
export class MarksForStudentComponent implements OnInit {

    constructor(private store: Store<State>, private markService: MarksService) { }
    user: any;
    listOfMarks: any;
    ngOnInit() {
        this.store.select('user').subscribe(data => {
            this.user = data.user;
            this.markService.getMarks(data.user).subscribe(marks => {
                this.listOfMarks = marks;
                
            });
        });
    }

    makeReclamationMark(markId:number){
        this.markService.makeReclamationMarks(this.user, markId).subscribe();
        this.markService.getMarks(this.user).subscribe(marks => {
            this.listOfMarks = marks;
        });
    }

    acceptMark(markId:number){
        this.markService.acceptMarks(this.user,markId).subscribe();
        this.markService.getMarks(this.user).subscribe(marks => {
            this.listOfMarks = marks;
        });
    }
}
