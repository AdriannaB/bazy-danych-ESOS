import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginService } from '../services/login.service';
import { State } from "../../store/app.store";
import { OnChanges, OnInit } from '@angular/core/src/metadata/lifecycle_hooks';


@Component({
    selector: 'student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnChanges, OnInit {

    constructor(private store: Store<State>) { }
    user: any;

    ngOnInit() {
        this.store.select('user')
            .subscribe((data: State) => {
                this.user = data.user;
            });
    }

    ngOnChanges() {
        this.store.select('user')
            .subscribe((data: State) => {
                this.user = data.user;
            });
    }
}
