import { Component } from '@angular/core';

@Component({
    selector: 'not-implemented',
    templateUrl: './not-implemented.component.html',
    styleUrls: ['./not-implemented.component.css']
})
export class NotImplementedComponent {

}
