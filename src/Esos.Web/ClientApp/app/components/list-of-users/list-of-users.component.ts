import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../services/user-list.service';
import { Store } from '@ngrx/store';
import { State } from "../../store/app.store";
import { AddUserService } from '../services/add-user.service';
import { SETUSERS } from "../../store/user/login.actions";
import * as $ from 'jquery';
import { EditUserService } from '../services/edit-user.service';

@Component({
    selector: 'list-of-users',
    templateUrl: './list-of-users.component.html',
    styleUrls: ['./list-of-users.component.css']
})
export class ListOfUsersComponent implements OnInit {
    constructor(
        private userService: UserService,
        private store: Store<State>,
        private addUserService: AddUserService,
        private editUserService: EditUserService,
        private rd: Renderer2) {}

    listOfUsers: any;
    user: any;
    userToEdit: any;
    @ViewChild('addUserModal') el: ElementRef;
    @ViewChild('editUserModal') elModalEdit: ElementRef;

    roles:{ [key:number]:string }= {
        1: 'Student',
        2: 'Prowadzący',
        3: 'Administrator'
    };

    ngOnInit() {
        this.store.select('user').subscribe(data => {
            this.user = data.user;
            this.userService.getUsers(data.user).subscribe(users => {
                this.listOfUsers = users;                
            });
        });
    }

    addUser( name:string, surname:string, password:string, secondName:string, PESEL:string, email:string, phoneNumber:string, birth:string, score:string, role:string){

        if(name == "" ) {
            this.el.nativeElement.children[0].children[1].style.border = '1px red solid';

        }
        if(surname == "" ) {
            this.el.nativeElement.children[1].children[1].style.border = '1px red solid';
            
        }
        if(password == "" ) {
            this.el.nativeElement.children[2].children[1].style.border = '1px red solid';
            
        }
        if(PESEL == "" ) {
            this.el.nativeElement.children[4].children[1].style.border = '1px red solid';
            
        }
        if(email == "" ) {
            this.el.nativeElement.children[5].children[1].style.border = '1px red solid';
            
        }
        if(phoneNumber == "" ) {
            this.el.nativeElement.children[6].children[1].style.border = '1px red solid';
            
        }
        if(birth == "" ) {
            this.el.nativeElement.children[7].children[1].style.border = '1px red solid';
            
        }
        if(role == "Wybierz Role" ) {
            this.el.nativeElement.children[9].children[1].style.border = '1px red solid';
            
        }
        if(name == "" || surname == "" || password == "" || PESEL == "" || email == "" || score == "" || role == ""){
            return; 
        }
        let userToAdd = {
            "Login": name+surname,
            "Password": password,
            "FirstName":name,
            "SecondName":secondName,
            "LastName":surname,
            "PESEL":phoneNumber,
            "Role": parseInt(role),
            "Indeks": "123456",
            "Email":email,
            "PhoneNumber":phoneNumber,
            "DateOfBirth":birth,
            "Score":parseInt(score)
        };
        
        this.addUserService.addUser(this.user, userToAdd).subscribe();


        this.clearModal();   
        
        
        this.userService.getUsers(this.user).subscribe(users => {
            this.listOfUsers = users;                
        }); 
    }

    editUser( name:string, surname:string, secondName:string, PESEL:string, email:string, phoneNumber:string, birth:string, score:string){

        if(name == "" ) {
            this.elModalEdit.nativeElement.children[0].children[1].style.border = '1px red solid';

        }
        if(surname == "" ) {
            this.elModalEdit.nativeElement.children[1].children[1].style.border = '1px red solid';
            
        }
        if(PESEL == "" ) {
            this.elModalEdit.nativeElement.children[3].children[1].style.border = '1px red solid';
            
        }
        if(email == "" ) {
            this.elModalEdit.nativeElement.children[4].children[1].style.border = '1px red solid';
            
        }
        if(phoneNumber == "" ) {
            this.elModalEdit.nativeElement.children[5].children[1].style.border = '1px red solid';
            
        }
        if(birth == "" ) {
            this.elModalEdit.nativeElement.children[6].children[1].style.border = '1px red solid';
            
        }
        if(score == "" ) {
            this.elModalEdit.nativeElement.children[7].children[1].style.border = '1px red solid';
            
        }
        if(name == "" || surname == "" || PESEL == "" || email == "" || score == ""){
            return; 
        }
        let userToEdit = {
            "ID": this.userToEdit.ID,
            "Login": this.userToEdit.Login,
            "Password": this.userToEdit.Password,
            "FirstName":name,
            "SecondName":secondName,
            "LastName":surname,
            "PESEL":PESEL,
            "Role": this.userToEdit.Role,
            "Indeks": "123456",
            "Email":email,
            "PhoneNumber":phoneNumber,
            "DateOfBirth":birth,
            "Score":parseInt(score)
        };
        this.editUserService.editUser(this.user, userToEdit).subscribe(()=>{
        });

        this.userService.getUsers(this.user).subscribe(users => {
            this.listOfUsers = users;                
        }); 

        for (let i = 0; i < 8; i++) {
            this.elModalEdit.nativeElement.children[i].children[1].value = '';
        }

        $('#cancelEdit').click();
    }

    setUserToEdit(index:number){
        if(this.listOfUsers){
            this.userToEdit = this.listOfUsers[index];
        }
            this.elModalEdit.nativeElement.children[0].children[1].value = this.userToEdit.FirstName;

            this.elModalEdit.nativeElement.children[1].children[1].value = this.userToEdit.LastName;
   
            this.elModalEdit.nativeElement.children[3].children[1].value = this.userToEdit.PESEL;
            
            this.elModalEdit.nativeElement.children[4].children[1].value = this.userToEdit.Email;
            
            this.elModalEdit.nativeElement.children[5].children[1].value = this.userToEdit.PhoneNumber;
            
            this.elModalEdit.nativeElement.children[6].children[1].value = this.userToEdit.DateOfBirth;
            
            this.elModalEdit.nativeElement.children[7].children[1].value = this.userToEdit.Score;

    }

    getRole(role:number){
        return this.roles[role];
    }

    clearModal() {
       $('#cancel').click();
        for (let i = 0; i < 9; i++) {
            this.el.nativeElement.children[i].children[1].value = '';
        }

    }

}

