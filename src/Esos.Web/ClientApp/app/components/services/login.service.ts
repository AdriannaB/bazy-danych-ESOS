import { Injectable ,OnInit,OnDestroy} from '@angular/core';
import { Http, Response,Headers,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

    baseUrl:string='http://localhost:5000/api/Account/LogIn/';
    
    constructor(public http:Http) { }
    
    logIn(data: {Login: string, Password: string}) {
     let headers = new Headers({ 'Content-Type': 'application/json'});
     return this.http.post(this.baseUrl,data ,{ headers })
     .map(result=>result.json());
     }
}
