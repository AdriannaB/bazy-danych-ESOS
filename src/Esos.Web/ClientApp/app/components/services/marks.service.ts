import { Injectable ,OnInit,OnDestroy} from '@angular/core';
import { Http, Response,Headers,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Request } from '@angular/http/src/static_request';

@Injectable()
export class MarksService {
    
    constructor(public http:Http) { }

    getMarks (Requester: any) {
     let url = 'http://localhost:5000/api/Student/GetStudentMarks/';
     let headers = new Headers({ 'Content-Type': 'application/json'});
     let id = Requester.ID;
     return this.http.post(url,{Requester, "StudentId":id},{ headers })
     .map((result) => result.json());
     }

     
    acceptMarks (Requester: any, markId:number) {
        let url = 'http://localhost:5000/api/Student/AcceptMark/';
        let headers = new Headers({ 'Content-Type': 'application/json'});
        return this.http.post(url,{Requester, "MarkId":markId},{ headers })
        .map((result) => result.json());
    }

    makeReclamationMarks (Requester: any, markId:number) {
        let url = 'http://localhost:5000/api/Student/Complainmark/';
        let headers = new Headers({ 'Content-Type': 'application/json'});
        return this.http.post(url,{Requester, "MarkId":markId},{ headers })
        .map((result) => result.json());
    }
}
