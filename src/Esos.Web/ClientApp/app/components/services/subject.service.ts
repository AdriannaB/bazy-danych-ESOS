import { Injectable ,OnInit,OnDestroy} from '@angular/core';
import { Http, Response,Headers,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { ReactiveFormsModule } from '@angular/forms';

@Injectable()
export class SubjectService {
    
    constructor(public http:Http) { }

    getSubjects (Requester: any) {
     let url = 'http://localhost:5000/api/Student/GetStudentActivities/';
     let headers = new Headers({ 'Content-Type': 'application/json'});
     let studentId = Requester.ID;
     return this.http.post(url,{Requester, "StudentId":studentId},{ headers })
     .map((result) => result.json());
     }

     getSubjectForLecturer(Requester:any){
        let url = 'http://localhost:5000/api/Lecturer/GetLecturerActivities/';
        let headers = new Headers({ 'Content-Type': 'application/json'});
        let lecturerId = Requester.ID;
        return this.http.post(url,{Requester, "LecturerId":lecturerId},{ headers })
        .map((result) => result.json());
        }
     }

