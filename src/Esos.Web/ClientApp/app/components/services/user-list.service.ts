import { Injectable ,OnInit,OnDestroy} from '@angular/core';
import { Http, Response,Headers,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
    
    constructor(public http:Http) { }

    getUsers (Requester: any) {
     let url = 'http://localhost:5000/api/Administration/GetUsers/';
     let headers = new Headers({ 'Content-Type': 'application/json'});
     return this.http.post(url,{Requester},{ headers })
     .map((result) => result.json());
     }
}
