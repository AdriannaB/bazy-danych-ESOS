import { Injectable ,OnInit,OnDestroy} from '@angular/core';
import { Http, Response,Headers,RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class EditUserService {
    
    constructor(public http:Http) { }

    editUser (Requester: any, User: any) {
        let url = 'http://localhost:5000/api/Administration/EditUser/';
        let headers = new Headers({ 'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(url,{Requester, User},options).map((res) => res.json());
    }
}

