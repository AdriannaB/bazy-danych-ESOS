import { Component } from '@angular/core';
import { OnChanges, OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

import { Store } from '@ngrx/store';
import { State } from "../../store/app.store";
@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent  implements OnInit, OnChanges{
    constructor(private store: Store<State>) { }
    userRole: number = 0;
    
    ngOnInit(){
        this.store.select('user')
        .subscribe((data: State) => {
            this.userRole = data.user.Role;
        });
    }

    ngOnChanges() {
        this.store.select('user')
        .subscribe((data: State) => {
            this.userRole = data.user.Role;
        });
    }
}
