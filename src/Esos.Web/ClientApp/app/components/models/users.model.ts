export interface Mark {
    Activity: Activity;
    User:User;
    Value: number;
    IsAccepted: boolean;
}

export interface Faculty {
    Name: string;
    Threshold: number;
}

export interface Subject {
    Name: string;
    ECTS: number;
    Semester: number;
    Faculty: Faculty;
}

export interface Building {
    Name: string;
    Street: string;
    Number: number;
    PostCode: string;
    City: string;
}

export interface Room {
    Building: Building;
    StudentsLimit: number;
    RoomNumber: number;
    FloorNumber: number;
}

export interface Activity {
    Lecturer: User;
    Subject: Subject;
    Room: Room;
    Type: number;
}
export interface User {
    Login: string;
    Password: string;
    FirstName: string; 
    SecondName: string;
    LastName: string;
    PESEL: string;
    Role: number;
    Indeks : string;
    Email : string;
    PhoneNumber : string;
    DateOfBirth : string;
    Score : number;
    Activities: string[];
}
        
