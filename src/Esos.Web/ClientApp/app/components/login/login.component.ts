import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user-list.service';
import { ElementRef, Renderer2, ViewChild } from '@angular/core';
import { State } from "../../store/app.store";
import { SETUSER } from "../../store/user/login.actions";
import { Router } from '@angular/router';


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    constructor(private _loginService: LoginService,
        private store: Store<State>,
        private rd: Renderer2,
        private _router: Router,
        private userService: UserService) { this.router = _router }

    router: Router;
    user: any;
    succesLogIn: boolean = false;
    @ViewChild('loginField') el: ElementRef;
    @ViewChild('passwordField') elP: ElementRef;

    login(Login: string, Password: string) {
        
        this._loginService.logIn({ Login, Password }).subscribe(data => {
            this.store.dispatch({ type: SETUSER, payload: data });

            this.store.select('user')
                .subscribe((data: State) => {
                    this.user = data.user;
                    if (this.user) {
                        this.succesLogIn = true
                        this.showHome();
                    }
                    else {
                        this.showErrorMessage();
                    }
                });
        });
    }

    showErrorMessage() {
        this.el.nativeElement.style.border = '1px red solid';
        this.elP.nativeElement.style.border = '1px red solid';
    }

    showHome() {
        if(this.user.Role === 2) {
        this.router.navigate(['/home']);
        }
        if(this.user.Role === 3) {
            this.router.navigate(['/administrator']);
        }
        if(this.user.Role === 1) {
            this.router.navigate(['/student']);
        }
    }
}
    