import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoginService } from '../services/login.service';
import { State } from "../../store/app.store";
import { OnChanges, OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { SubjectService } from '../services/subject.service';


@Component({
    selector: 'subject-for-student',
    templateUrl: './subject-for-student.component.html',
    styleUrls: ['./subject-for-student.component.css']
})
export class SubjectForStudentComponent implements OnInit {

    constructor(private store: Store<State>, private subjectService: SubjectService) { }
    user: any;
    listOfSubjects: any;
    ngOnInit() {
        this.store.select('user').subscribe(data => {
            this.user = data.user;
            this.subjectService.getSubjects(data.user).subscribe(subjects => {
                this.listOfSubjects = subjects;  
            });
        });
    }

}
