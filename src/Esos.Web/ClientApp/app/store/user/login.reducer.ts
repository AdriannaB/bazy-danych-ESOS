import {ActionReducer, Action} from "@ngrx/store";
import { State, intitialState } from "../app.store";
import { SETUSER, SETUSERS } from "../user/login.actions";

export const loginReducer: ActionReducer<State> =
(state = intitialState, action:any) => {
  switch (action.type) {
    case SETUSER: {
      return {
        user: action.payload,
        users: state.users
      }
    }
    case SETUSERS: {
      return {
        user: state.user,
        users: action.payload
      }
    }
    default: {
      return state;
    }
  }
};