using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class GetStudentMarksRequest : AbstractRequest
    {
        public int StudentId {get;set;}
    }
}