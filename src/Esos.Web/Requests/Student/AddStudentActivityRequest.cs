using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class AddStudentActivityRequest : AbstractRequest
    {
        public int StudentId { get; set; }
        public int ActivityId { get; set; }
    }
}