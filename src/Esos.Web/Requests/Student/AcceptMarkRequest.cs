using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class AcceptMarkRequest : AbstractRequest
    {
        public int MarkId {get;set;}
    }
}