using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class GetAvailableActivitiesRequest : AbstractRequest
    {
        public int FacultyId {get;set;}
    }
}