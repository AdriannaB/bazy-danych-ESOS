using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class GetStudentActivitiesRequest : AbstractRequest
    {
        public int StudentId { get; set;}
    }
}