using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class ComplainMarkRequest : AbstractRequest
    {
        public int MarkId {get;set;}
    }
}