using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Student
{
    public class DeleteStudentActivityRequest : AbstractRequest
    {
        public int StudentId { get; set; }
        public int ActivityId { get; set; }
    }
}