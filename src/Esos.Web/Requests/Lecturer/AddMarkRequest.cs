using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Lecturer
{
    public class AddMarkRequest : AbstractRequest
    {
        public int StudnetId {get;set;}
        public int ActivityId {get;set;}
        public int Value {get;set;}
    }
}