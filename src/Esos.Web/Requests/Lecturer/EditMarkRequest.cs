using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Lecturer
{
    public class EditMarkRequest : AbstractRequest
    {
        public int MarkId {get;set;}
        public float Value {get;set;}
    }
}