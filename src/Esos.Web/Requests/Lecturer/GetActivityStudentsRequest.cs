using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Lecturer
{
    public class GetActivityStudentsRequest : AbstractRequest
    {
        public int ActivityId { get; set;}
    }
}