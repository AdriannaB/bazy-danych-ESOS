using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Lecturer
{
    public class GetLecturerActivitiesRequest : AbstractRequest
    {
        public int LecturerId {get;set;}
    }
}