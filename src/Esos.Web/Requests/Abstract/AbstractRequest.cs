using Esos.Web.Infrastructure.DTO;

namespace Esos.Web.Requests.Abstract
{
    public class AbstractRequest
    {
        public User Requester {get;set;}
    }
}