using Esos.Web.Infrastructure.DTO;
using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Administration
{
    public class AddActivityRequest : AbstractRequest
    {
        public Activity Activity { get; set; }
    }
}