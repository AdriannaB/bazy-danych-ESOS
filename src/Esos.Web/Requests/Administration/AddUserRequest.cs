using Esos.Web.Infrastructure.DTO;
using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Administration
{
    public class AddUserRequest : AbstractRequest
    {
        public User User {get;set;}
    }
}