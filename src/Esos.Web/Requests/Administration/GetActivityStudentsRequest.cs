using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Administration
{
    public class GetActivityStudentsRequest : AbstractRequest
    {
        public int ActivityId { get; set; }
    }
}