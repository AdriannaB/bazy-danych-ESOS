using Esos.Web.Requests.Abstract;

namespace Esos.Web.Requests.Administration
{
    public class AddStudentActivityRequest:AbstractRequest
    {
        public int StudentId { get; set; }
        public int ActivityId { get; set; }

    }
}