namespace Esos.Web.Requests.Account
{
    public class LoginRequest
    {
        public string Login {get;set;}
        public string Password {get;set;}
    }
}