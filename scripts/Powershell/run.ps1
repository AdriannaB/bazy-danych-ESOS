$currentLocation=$PSScriptRoot
$buildLocation="../.."
$webLocation="../../src/Esos.Web"
$runLocation="src/Esos.Web"
$node="npm install"
$restore="dotnet restore"
$build="dotnet build"
$run="dotnet run"
Set-Location $webLocation
iex $node
Set-Location $buildLocation
iex $restore
iex $build
Set-Location $runLocation
$env:ASPNETCORE_ENVIRONMENT="Development"
iex $run
Set-Location $currentLocation
